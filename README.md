# wpoison GT

The original [wpoison](https://web.archive.org/web/20160821195248/http://www.monkeys.com:80/wpoison/), by Ronald F. Guilmette, is a CGI script that generates random web content containing made-up e-mail addresses; the idea is that spammers' web crawlers, which trawl web sites looking for e-mail addresses, stumble upon it and start adding these made-up e-mail addresses to their lists. This either forces the spammer to dump the entire list or manually clean it (if they notice the poisoning) or waste their resources trying to spam non-existent e-mail addresses (if they don't).

However, the original version suffers from a number of problems:

1. The content generated is in a fixed format which is trivial to identify.
1. The usage restrictions specify that all installations must have a link to the wpoison site on their homepage (although it is no longer accessible); this makes it trivial for a crawler to skip any sites that have this.
1. It is possible to determine if the script is in use by requesting a made-up URL inside the wpoison 'site' - wpoison will return content for anything, and never return a 404 error.
1. Requesting the same page several times results in totally different content each time, unlike a real site.
1. The content generated is devoid of punctuation and other formatting, with a limited set of HTML markup, making it easier to identify as bogus.
1. There is no limit to the amount of e-mail addresses the script will issue to a single crawler; a sudden large number of new e-mail addresses may arouse the spammers' suspicion early on.
1. Also because there is no limit, badly-written but legitimate crawlers may get 'stuck' in the 'site', increasing the load on the server unnecessarily.

So, in light of this, I produced an amended version, *wpoison-gt*, which does not suffer from issues 1 and 5. Full changes:

* The order of output of words, links and e-mail addresses is now randomised in the response.
* The various parts of the `<head>` are included/excluded at random (apart from the robots `meta` tag, which is always included), and output in a random order.
* Output includes various HTML tags so it looks more like a real page; there's also some randomly-sprinkled punctuation.
* Indents and newlines in head and body are randomised.
* Output is now more-or-less-valid XHTML, styled with CSS.
* We now `sleep()` for a short time before any of the body is output, rather than right at the end, just in case over-zealous crawlers start fetching the links we send before we've finished output. There's also occasional random `sleep()`s while we do output.

However, the original author is no longer maintaining wpoison, and did not wish to incorporate my modifications, so I am distributing it here. This script is based on the original wpoison code, and is suitable for use as a drop-in replacement, so the original [installation instructions](https://web.archive.org/web/20010224175549/http://www.monkeys.com:80/wpoison/installing.html) should all still apply. Please note the original licence still stands (this release is not made under the GPL), including the requirement to link to the original wpoison site (it was [here](http://www.monkeys.com/wpoison/), for what it's worth, but is now dead).

You can see [this version of the script in action](http://www.gloomytrousers.co.uk/tainted/) - the output has a pleasing [Vogon poetry](https://en.wikipedia.org/wiki/Vogon_poetry) feel to it.
